let formatLength = (line)=> {
  var length = ol.sphere.getLength(line);
  var output;
  if (length > 100) {
    output = (Math.round(length / 1000 * 100) / 100) +
    ' ' + 'km';
  } else {
    output = (Math.round(length * 100) / 100) +
    ' ' + 'm';
  }
  return output;
};

let formatDistance = (firstCoordinates, pointCoordinate)=> {
  let distance = ol.sphere.getDistance(firstCoordinates, pointCoordinate)
  let output;
  if (distance > 100) {
    output = (Math.round(distance / 1000 * 100) / 100) +
    ' ' + 'km';
  } else {
    output = (Math.round(distance * 100) / 100) +
    ' ' + 'm';
  }
  return output;
};


export {
  formatLength,
  formatDistance
}