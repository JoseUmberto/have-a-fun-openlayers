/*
  Imports
*/
import Map from 'ol/Map.js';
import View from 'ol/View.js';
import Feature from 'ol/Feature';
import { Draw, Modify, Snap, Select } from 'ol/interaction.js';
import { Tile as TileLayer, Vector as VectorLayer } from 'ol/layer.js';
import { OSM, Vector as VectorSource } from 'ol/source.js';
import { Stroke, Style } from 'ol/style.js';
import { LineString, Point } from 'ol/geom.js';
import { click, pointerMove } from 'ol/events/condition.js';
import GeoJSON from 'ol/format/GeoJSON';
import * as random from "./utils/random-strings.js"
import * as calculus from "./utils/geometric-calculus.js"
import Overlay from 'ol/Overlay.js';

//======================================================================================================


/*
  Get Elements from document
*/
const form = document.getElementById("type")
const alert = document.getElementById('alert');
const exportJSON = document.getElementById('link')
const file = document.getElementById("file-input")
//======================================================================================================

/*
  Get Elements from document
*/
let msgAlert = document.createElement('p');

//======================================================================================================


/*
  several objects
*/
let featureAgregation = {};
let sketch;
//======================================================================================================

/*
  Open Layers Objects
*/
const raster = new TileLayer({
  source: new OSM()
});

const source = new VectorSource();

const format = new GeoJSON()

const vector = new VectorLayer({
  source: source,
  style: new Style({
    stroke: new Stroke({
      color: '#ffcc33',
      width: 2
    })
  })
});

const map = new Map({
  layers: [raster, vector],
  target: 'map',
  view: new View({
    center: [-5483129.009695, -1883881.331059],
    projection: 'EPSG:3857',
    zoom: 15
  })
});

let draw = new Draw({
  source: source,
  type: 'LineString',
  style: new Style({
    stroke: new Stroke({
      color: 'rgba(0, 0, 0, 0.5)',
      lineDash: [10,10],
      width: 2
    })
  })
});

const select = new Select({
  condition: pointerMove
})

const modify = new Modify({
  source: source,
  features: select.getFeatures()
});

const snap = new Snap({source: vector.getSource()});

map.addInteraction(draw);
map.addInteraction(select);
map.addInteraction(modify);
map.addInteraction(snap);

draw.setActive(false)
select.setActive(false)
modify.setActive(false)
//======================================================================================================

/*
  Events Callback
*/

const formHandling = ()=>{
  
  if(alert.childNodes.length > 0){
    while (alert.firstChild) {
      alert.removeChild(alert.firstChild);
    }
  }
  
  let radioValue = document.querySelector('input[name="typeRadio"]:checked').value;

  if(radioValue !== "draw"){
    if(Object.entries(featureAgregation).length <= 0){
      msgAlert.innerHTML = "Não há aminhos para selecionar! Desenhe um."
      alert.appendChild(msgAlert)
      alert.insertAdjacentElement('afterbegin', msgAlert)  
    } else {
      modify.setActive(true)
      select.setActive(true)
      draw.setActive(false)
      modifyInteraction()
    }
  } else {
    select.setActive(false)
    modify.setActive(false)
    draw.setActive(true)
    drawInteraction()
  }
}
//======================================================================================================

/*
  Map interaction
*/
let measureTooltipElement;
let measureTooltip;

let measureDistanceElement;
let measureDistanceTip;

let createMeasureDistance = () =>{
  if (measureDistanceElement) {
    measureDistanceElement.parentNode.removeChild(measureDistanceElement);
  }
  measureDistanceElement = document.createElement('div');
  measureDistanceElement.className = 'tooltip tooltip-measure';

  measureDistanceTip = new Overlay({
    element: measureDistanceElement,
    offset: [0, -15],
    positioning: 'bottom-center'
  })
  map.addOverlay(measureDistanceTip);
}


let createMeasureToolTip = ()=>{
  /*
  if (measureTooltipElement) {
    measureTooltipElement.parentNode.removeChild(measureTooltipElement);
  }
  */
  measureTooltipElement = document.createElement('div');
  
  measureTooltipElement.className = 'tooltip tooltip-static';
  measureTooltip = new Overlay({
    element: measureTooltipElement,
    offset: [0, -15],
    positioning: 'bottom-center'
  });
  map.addOverlay(measureTooltip);
}

//======================================================================================================

/*
  Map interaction
*/
let drawInteraction = ()=>{
  
  createMeasureToolTip()
  
  draw.on('drawstart', function(drawStartEvent){
    
  })

  draw.on('drawend', function(drawEndEvent) {
    let sketch_2 = drawEndEvent.feature;
    let options = {units: 'meters'};
    let rotas = {}
    var turfLine = format.writeFeatureObject(sketch_2);

    let coordinates = turfLine.geometry.coordinates;
    let firstPoint = coordinates[0];

    for(let i = 0; i < coordinates.length; i++){
      if(coordinates[i] == firstPoint){
        console.log("PONTO INICIAL");
        continue
      }
      let lastPoint = coordinates[i]; 
      var distance = turf.distance(turf.point(firstPoint), turf.point(lastPoint), options);
      rotas["Rotas "+i] = {
        "Distancia": distance,
        "Ponto": turf.point(lastPoint)
      };
    }

    for (var property in rotas) {
      if (rotas.hasOwnProperty(property)) {
        console.log("A distância é ", rotas[property].Ponto);
        let marker = format.readFeature(rotas[property].Ponto);
        marker.setStyle(
          new Style({
            stroke: new Stroke({
              color: '#FF0000',
            })
          }
        ))
        console.log("MARKER", marker)
        console.log("LAYOUT", marker)

        marker.getGeometry().transform('EPSG:4326', 'EPSG:3857');
        source.addFeature(marker); 
      }
    }

    let getFe = source.getFeatures()
  })
}

let modifyInteraction = ()=> {
  
  let firstCoordinate = null;

  select.on('select', function(selectEvent) {
    let feature = selectEvent.selected[0];
    try{

      if(feature['id_'] && (feature.getGeometry() instanceof LineString)){
        firstCoordinate = feature.getGeometry().getFirstCoordinate();
        //console.log('firstCoordinate', firstCoordinate)
      }

      feature.getGeometry().on('change', (evenf)=>{
        createMeasureDistance()
        if(evenf.target instanceof Point){
          if(firstCoordinate !== null){
            let pointCoordiante = evenf.target.getCoordinates();
            let distance = calculus.formatDistance(firstCoordinate, pointCoordiante);
            measureDistanceElement.innerHTML = distance;
            measureDistanceTip.setPosition(pointCoordiante);
          }
        }
      })

    }catch(error){
      console.log("Undefined vector layer",)
    }
  });

  modify.on('modifystart', function(modifyStartEvent){
  })
  
  modify.on('modifyend', function(modifyEndEvent){
    let features = modifyEndEvent.features;
    features.forEach(function(feature){
      console.log("FEATURE", feature)
      if(featureAgregation[feature.getId()]){        
        featureAgregation[feature.getId()].geometry.coordinates = feature.getGeometry().getCoordinates();
        featureAgregation[feature.getId()].Style.Stroke.color_ = feature.getStyle().getStroke().getColor();
        featureAgregation[feature.getId()].Style.Stroke.width_ = feature.getStyle().getStroke().getWidth();        
      }
    })
  })
}
//======================================================================================================
function getJSON(){
  let dataStr = "data:text/json;charset=utf-8," + encodeURIComponent(JSON.stringify(featureAgregation));
  exportJSON.setAttribute("href", dataStr);
  exportJSON.setAttribute("download", "LineString.json");
}


/*
  Event Listeners
*/
form.addEventListener('click', formHandling);
exportJSON.addEventListener('click', getJSON)

//======================================================================================================